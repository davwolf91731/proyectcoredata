//
//  ViewController.swift
//  ProyectoCoreData
//
//  Created by david tayupanta on 26/1/18.
//  Copyright © 2018 david tayupanta. All rights reserved.
//

import UIKit
import CoreData
class ViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    var persona: Person?
    
    private let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate) .persistentContainer.viewContext
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    

    @IBAction func saveButtonPressed(_ sender: Any) {
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: managedObjectContext)
        let person = Person (entity: entityDescription!, insertInto: managedObjectContext)
        person.name = nameTextField.text!
        person.address = addressTextField.text!
        person.phone = phoneTextField.text!
        do {
            try managedObjectContext.save()
            clearfields()
        } catch {
            print("Error al guardar")
        }
        
    }
    func clearfields(){
        nameTextField.text = ""
        addressTextField.text=""
        phoneTextField.text=""
    }
    
    @IBAction func findButtonPressed(_ sender: Any) {
        if nameTextField.text == "" {
            performSegue(withIdentifier: "fineALL", sender: self)
            return
        }else{
            fetchByName()
        }
        let request: NSFetchRequest<Person> = Person.fetchRequest()
        let pred = NSPredicate(format: "name = %@", nameTextField.text!)
        request.predicate = pred
        do {
           let results = try managedObjectContext.fetch(request)
            if results.count > 0 {
                let person = results[0]
                addressTextField.text = person.address
                phoneTextField.text = person.phone
            }
        } catch let error  {
            print("error")
        }
    }
    
    func fetchAll(){
        let request : NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let results = try managedObjectContext.fetch(request)
            for p in results {
                let person = p as! Person
                print ("Nombre: \(person.name!) --", terminator:"")
                print ("Telefono: \(person.phone!) --", terminator:"")
                print ("Direccion: \(person.phone!) --", terminator:"")
            }
        } catch  {
            print("error al mostrar")
        }
    }
    func fetchAllTable(){
        let request : NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let results = try managedObjectContext.fetch(request)
            for p in results {
                let person = p as! Person
                
                print ("Nombre: \(person.name!) --", terminator:"")
                print ("Telefono: \(person.phone!) --", terminator:"")
                print ("Direccion: \(person.phone!) --", terminator:"")
            }
        } catch  {
            print("error al mostrar")
        }
    }
    
    func fetchByName(){
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        let pred = NSPredicate(format: "name = %@", nameTextField.text!)
        request.predicate = pred
        do{
            let results = try managedObjectContext.fetch(request)
            if results.count > 0{
                persona = results[0] as! Person
                nameTextField.text = persona?.name
                addressTextField.text = persona?.address
                phoneTextField.text = persona?.phone
                
                performSegue(withIdentifier: "findOne", sender: self)
            } else {
                addressTextField.text = ""
                phoneTextField.text = ""
                performSegue(withIdentifier: "segueTodos", sender: self)
            }
        }catch{
            print("Error al consultar por nombre")
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "findOne"){
            let destination = segue.destination as! fineOneViewController
            destination.persona = persona
        }
        if(segue.identifier == "fineALL" ){
            let destination = segue.destination as! fineALLViewController
            
        }
    }
}

