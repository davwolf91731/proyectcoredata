//
//  fineALLViewController.swift
//  ProyectoCoreData
//
//  Created by david tayupanta on 2/2/18.
//  Copyright © 2018 david tayupanta. All rights reserved.
//

import UIKit
import CoreData

class fineALLViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    private let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate) .persistentContainer.viewContext
    var personas: [Person] = []
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return personas.count
        default:
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PersonaCell") as! PersonaTableViewCell
        cell.fillData(personas[indexPath.row])
        return cell
    }
    

    @IBOutlet weak var tablaPersonas: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
print(personas)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func fetchAll(){
        let request : NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let results = try managedObjectContext.fetch(request)
            personas = results
            for p in results {
                let person = p as! Person
                print ("Nombre: \(person.name!) --", terminator:"")
                print ("Telefono: \(person.phone!) --", terminator:"")
                print ("Direccion: \(person.phone!) --", terminator:"")
            }
        } catch  {
            print("error al mostrar")
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func viewWillAppear(_ animated: Bool) {
        fetchAll()
    }
    func listaPersona(_ persona: [Person]) {
        personas = persona
        tablaPersonas.reloadData()
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
