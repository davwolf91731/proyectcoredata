//
//  PersonaTableViewCell.swift
//  ProyectoCoreData
//
//  Created by david tayupanta on 2/2/18.
//  Copyright © 2018 david tayupanta. All rights reserved.
//

import UIKit

class PersonaTableViewCell: UITableViewCell {
    
    @IBOutlet weak var addresLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var phoneLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func fillData(_ person: Person){
        nameLabel.text = person.name ?? "nd"
        addresLabel.text = person.address ?? "nd"
        phoneLabel.text = person.phone ?? "nd"
        
        
    }

}
