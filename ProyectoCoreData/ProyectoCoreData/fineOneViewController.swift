//
//  fineOneViewController.swift
//  ProyectoCoreData
//
//  Created by david tayupanta on 2/2/18.
//  Copyright © 2018 david tayupanta. All rights reserved.
//

import UIKit

class fineOneViewController: UIViewController {
    @IBOutlet weak var direccionInput: UITextField!
    var persona : Person?
    @IBOutlet weak var telefonoInput: UITextField!
    private let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
   
    override func viewDidLoad() {
        super.viewDidLoad()
        direccionInput.text = persona?.address
        telefonoInput.text = persona?.phone

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func borrar(_ sender: Any) {
        managedObjectContext.delete(persona!)
        
        do {
            try managedObjectContext.save()
            navigationController?.popViewController(animated: true)
        } catch {
            print("Error al eliminar")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
